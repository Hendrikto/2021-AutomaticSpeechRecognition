from tensorflow import keras
from tensorflow.keras.layers import (
    AveragePooling1D,
    Conv1D,
    Conv1DTranspose,
    Dense,
    Dropout,
    Flatten,
    Input,
    Reshape,
    UpSampling1D,
)


def build_encoder(input_shape, n_blocks=3):
    audio = Input(dtype='float32', name='audio', shape=input_shape)
    x = Reshape((-1, 1))(audio)

    for i in reversed(range(1, n_blocks + 1)):
        x = Conv1D(2 ** i, kernel_size=5, padding='same')(x)
        x = AveragePooling1D()(x)

    x = Conv1D(1, kernel_size=1, padding='same')(x)
    code = Flatten(name='code')(x)

    return keras.Model(name='encoder', inputs=audio, outputs=code)


def build_decoder(input_shape, n_blocks=3):
    code = Input(dtype='float32', name='code', shape=input_shape)
    x = Reshape((-1, 1))(code)

    for i in range(1, n_blocks + 1):
        x = UpSampling1D()(x)
        x = Conv1DTranspose(2 ** i, kernel_size=5, padding='same')(x)

    x = Conv1DTranspose(1, kernel_size=1, padding='same')(x)
    audio = Flatten(name='audio')(x)

    return keras.Model(name='decoder', inputs=code, outputs=audio)


def build_autoencoder(encoder, decoder):
    original = encoder.input
    code = encoder(original)
    reproduction = decoder(code)

    return keras.Model(name='autoencoder', inputs=original, outputs=reproduction)


def build_classifier(encoder, dense_size=256):
    audio = encoder.input

    code = encoder(audio)

    x = Dense(dense_size, activation='relu')(code)
    x = Dropout(0.25)(x)
    digit = Dense(10, activation='softmax')(x)

    return keras.Model(name='classifier', inputs=audio, outputs=digit)
