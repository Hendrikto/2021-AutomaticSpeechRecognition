from functools import partial
from itertools import product

import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds

from . import config

π = np.pi


def sine(
    amplitude=1,
    frequency=1,
    phase=0,
    *,
    nr_samples=config.AUDIO_LENGTH,
    sample_rate=config.SAMPLE_RATE,
):
    seconds = nr_samples / sample_rate
    time = np.linspace(0, seconds * 2 * π, num=nr_samples)
    return amplitude * np.sin(frequency * time + phase)


def sine_generator(
    amplitudes=(1,),
    frequencies=(1,),
    phases=(0,),
    **kwargs,
):
    for sine_properties in product(amplitudes, frequencies, phases):
        yield sine(*sine_properties, **kwargs)


def sine_dataset_factory(**kwargs):
    audio_shape = kwargs.get('nr_samples', config.AUDIO_LENGTH)

    return tf.data.Dataset.from_generator(
        partial(sine_generator, **kwargs),
        output_signature=tf.TensorSpec(shape=audio_shape, dtype=tf.float32),
    )


def _norm(audio, label):
    audio = tf.cast(audio, tf.float32)
    label = tf.cast(label, tf.int32)

    max_magnitude = tf.reduce_max(tf.abs(audio))
    audio = audio / max_magnitude

    return audio, label


def spoken_digit_dataset_factory(
    nr_samples=config.AUDIO_LENGTH,
    tfds_data_dir='data',
):
    dataset = tfds.load(
        'spoken_digit',
        as_supervised=True,
        data_dir=tfds_data_dir,
    )['train']

    return (
        dataset
        # filter out long datapoints
        .filter(lambda a, l: tf.shape(a)[0] <= nr_samples)
        # normalize and cast to correct types
        .map(_norm)
    )
