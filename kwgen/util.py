import math

import matplotlib.pyplot as plt
import numpy as np
import simpleaudio as sa
import tensorflow as tf

from . import config


def log1p_activation(x):
    return tf.math.log1p(tf.abs(x)) * tf.math.sign(x)


def moving_average(data, window_size):
    return np.convolve(data, np.ones(window_size), mode='valid') / window_size


def overlay(autoencoder, audios, *, rows=None, columns=None):
    n_audios = len(audios)
    if rows is None:
        rows = math.ceil(n_audios / (columns or 2))
    if columns is None:
        columns = math.ceil(n_audios / rows)

    reconstructions = autoencoder.predict(audios)

    plt.figure(figsize=(4 * columns, rows * 2))
    for i in range(n_audios):
        plt.subplot(rows, columns, i + 1)
        plt.ylim(-1, 1)
        plt.plot(audios[i].ravel(), label='original')
        plt.plot(reconstructions[i].ravel(), label='reconstruction')
    plt.legend()
    plt.tight_layout()
    plt.show()


def play(audio, wait_done=False):
    player = sa.play_buffer(
        audio_data=audio,
        bytes_per_sample=4,
        num_channels=1,
        sample_rate=config.SAMPLE_RATE,
    )

    if wait_done:
        player.wait_done()


def scale(data, base=10):
    """Scale data to scientific notation."""
    data = np.asarray(data)
    exponent = -math.floor(np.log10(data).max())
    return data * base ** exponent, exponent
