import matplotlib.pyplot as plt
import numpy as np


def evaluate(autoencoder, dataset, metric):
    return np.hstack([
        metric(batch, autoencoder.predict(batch))
        for batch in dataset
    ])


def draw_confusion_matrix(
    confusion_matrix,
    *,
    save_path=None,
    size=8,
    title=None,
):
    mean = np.mean(confusion_matrix)
    n_classes = len(confusion_matrix)

    # set up figure
    plt.figure(figsize=(size, size))
    if title is not None:
        plt.title(title)
    plt.xlabel('true label')
    plt.ylabel('predicted label')
    plt.xticks(range(n_classes))
    plt.yticks(range(n_classes))

    # draw confusion matrix
    plt.imshow(confusion_matrix)
    for y, row in enumerate(confusion_matrix):
        for x, value in enumerate(row):
            color = 'white' if value < mean else 'black'
            plt.text(
                x, y,
                f'{value}',
                color=color,
                horizontalalignment='center',
                verticalalignment='center',
            )
    plt.tight_layout()

    if save_path is not None:
        plt.savefig(save_path)

    plt.show()


def draw_metric_plot(
    metric_values,
    *,
    max_ticks=10,
    save_path=None,
    title=None,
    x_format='{:.3f}',
    x_label,
    x_size=9,
    x_values,
    y_format='{:.3f}',
    y_label,
    y_size=8,
    y_values,
):
    # set up figure
    plt.figure(figsize=(x_size, y_size))
    if title is not None:
        plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)

    # set axis tick labels
    x_n = len(x_values)
    x_ticks = np.linspace(0, x_n - 1, num=min(x_n, max_ticks))
    plt.xticks(x_ticks, map(x_format.format, np.interp(x_ticks, range(x_n), x_values)))
    y_n = len(y_values)
    y_ticks = np.linspace(0, y_n - 1, num=min(y_n, max_ticks))
    plt.yticks(y_ticks, map(y_format.format, np.interp(y_ticks, range(y_n), y_values)))

    # draw 2D metric plot
    plt.imshow(metric_values, aspect='auto')
    plt.colorbar()
    plt.tight_layout()

    if save_path is not None:
        plt.savefig(save_path)

    plt.show()
