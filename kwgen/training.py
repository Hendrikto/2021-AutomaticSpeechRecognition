from datetime import datetime

import numpy as np
from tensorflow import keras

from . import config
from .data import (
    sine_dataset_factory,
    spoken_digit_dataset_factory,
    π,
)
from .model import (
    build_autoencoder,
    build_classifier,
    build_decoder,
    build_encoder,
)


class LRTensorBoard(keras.callbacks.TensorBoard):
    """Tensorboard callback which keeps track of the learning rate."""
    def on_epoch_end(self, epoch, logs=None):
        logs = {} if logs is None else logs
        logs['lr'] = self.model.optimizer.lr
        super().on_epoch_end(epoch, logs)


def lr_schedule_factory(α=-2, β=1/3, γ=10, δ=2, η=0.001):
    def lr_schedule(epoch, learning_rate=None):
        e = np.e
        return np.min((
            e ** α / (e ** -(epoch/β) + e ** α),
            e ** γ / (e ** (epoch/δ) + e ** γ),
        ), axis=0) * η

    return lr_schedule


def train(
    model,
    dataset,
    *,
    # callback parameters
    callbacks=(),
    lr_schedule=lr_schedule_factory(α=-2, β=1, γ=12, δ=3),
    tensorboard_path='data/tensorboard/{timestamp}',
    # fit parameters
    epochs=50,
    **kwargs,
):
    callbacks = list(callbacks)

    if lr_schedule is not None:
        callbacks.append(keras.callbacks.LearningRateScheduler(lr_schedule))

    if tensorboard_path:
        callbacks.append(LRTensorBoard(
            tensorboard_path.format(timestamp=datetime.now().strftime('%Y_%m_%d-%H_%M')),
            histogram_freq=1,
        ))

    return model.fit(
        dataset,
        callbacks=callbacks,
        epochs=epochs,
        **kwargs,
    )


sine_dataset = (
    sine_dataset_factory(
        amplitudes=np.linspace(0.5, 1, num=6),
        frequencies=np.linspace(0, 400, num=401),
        phases=np.linspace(0, 2 * π, num=9),
    )
    .map(lambda sine: (sine, sine))
    .shuffle(2 * config.BATCH_SIZE)
    .batch(config.BATCH_SIZE)
    .prefetch(config.PREFETCH_BATCHES)
)

spoken_digit_dataset = (
    spoken_digit_dataset_factory()
    .shuffle(2 * config.BATCH_SIZE)
    .padded_batch(config.BATCH_SIZE, ((config.AUDIO_LENGTH,), ()))
    .prefetch(config.PREFETCH_BATCHES)
)

encoder = build_encoder(config.AUDIO_LENGTH)
decoder = build_decoder(encoder.output.shape[1:])
autoencoder = build_autoencoder(encoder, decoder)
classifier = build_classifier(encoder)

autoencoder.compile(
    loss=keras.losses.MeanSquaredError(),
    optimizer=keras.optimizers.Adam(),
)

classifier.compile(
    loss=keras.losses.SparseCategoricalCrossentropy(),
    metrics='accuracy',
    optimizer=keras.optimizers.Adam(),
)
