import tensorflow as tf

# work around CUDA error `tensorflow.python.framework.errors_impl.UnknownError: Failed to get
# convolution algorithm.` caused by `tensorflow/stream_executor/cuda/cuda_dnn.cc:336] Could not
# create cudnn handle: CUDNN_STATUS_INTERNAL_ERROR`
for gpu in tf.config.list_physical_devices('GPU'):
    tf.config.experimental.set_memory_growth(gpu, True)

__author__ = 'Hendrikto <hendrik.to@gmail.com>'
