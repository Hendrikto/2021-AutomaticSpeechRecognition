\documentclass[12pt]{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[
	backend=biber,
	urldate=iso,
	seconds=true, % required by urldate=iso
]{biblatex}
\usepackage{enumerate}
\usepackage[margin=5em]{geometry}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{subcaption}

\addbibresource{bibliography.bib}

\setlength\parindent{0em}
\setlength\parskip{1em}

\newcommand{\Loss}{\ensuremath{\mathcal{L}}}

\DeclareMathOperator{\sign}{sign}

\title{
	\textbf{Automatic Speech Recognition}\\
	Thesis: Raw Audio Autoencoding
}

\author{Hendrik Werner s4549775}

\begin{document}

\maketitle

\section{Introduction}
\label{sec:introduction}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\linewidth]{plots/ae-schema}
	\caption{General Autoencoder Schema}
	\label{fig:ae_schema}
\end{figure}

Autoencoders are very generic and valuable tools, which have been applied with great success across a wide range of diverse domains. They consist of two parts, as shown in figure \ref{fig:ae_schema}. The main idea is to perform feature extraction or dimensionality reduction, by forcing the input through a bottleneck and reconstructing it afterwards. In essence, inputs are projected onto a lower-dimensional representation before reconstructing the original high-dimensional representation. To attain high performance at this task, autoencoders learn to identify and extract essential information from the input, such that they can reproduce it from a lower-dimensional representation with maximum accuracy. With sufficient training, the resulting code is often universal enough to be used for other tasks then reconstructing the input.

Using basic autoencoders is very uncomplicated, because they can be trained unsupervised. They do not require labeled data or specialized loss functions. Examples of what should be reconstructed are sufficient to train them using some simple, well-known loss function, such as Mean Squared Error (MSE), by taking target = input. This means that the loss becomes $\Loss(\text{original}, \text{reconstruction}) = \Loss(\text{original}, M_\theta(\text{original}))$, which only depends on the original and the model $M_\theta$ itself, and can therefore be calculated straighforwardly without requiring labels or any other information.

Autoencoders have successfully been applied in fields including Dimensionality Reduction \autocite{wang2014:autoencoder:dimensionality_reduction} \autocite{wang2016:autoencoder:dimensionality_reduction}, Feature Extraction \autocite{xing2015:autoencoder:feature_extraction-image_classification} \autocite{meng2017:autoencoder:feature_extraction}, Anomaly Detection \autocite{sakurada2014:autoencoder:anomaly_detection}, Graph Clustering \autocite{wang2017:autoencoder:graph_clustering}, Image Denoising \autocite{burger2012:autoencoder:image_denoising:1} \autocite{burger2012:autoencoder:image_denoising:2} \autocite{gondara2016:autoencoder:image_denoising}, Image Classification \autocite{geng2015:autoencoder:image_classification} \autocite{xing2015:autoencoder:feature_extraction-image_classification} \autocite{luo2018:autoencoder:image_classification}, Image Generation and Manipulation \autocite{xu2019:autoencoder:image_generation} \autocite{shao2020:autoencoder:image_generation}, and countless more, reaching state-of-the-art performance on many of these tasks.

Despite their ease of use, great performance, and wide applicability, there is a surprising lack of research regarding the usage of autoencoders on audio data \autocite{feng2014:autoencoder:denoising-dereverberation}. Most existing literature is limited to audio Denoising \autocite{feng2014:autoencoder:denoising-dereverberation} \autocite{zhao2015:autoencoder:denoising} and Dereverberation \autocite{ishii2013:autoencoder:dereverberation}. One cause of this might be that the majority of Automatic Speech Recognition (ASR) research in the past has been focused on operating on some feature representation like Mel Frequency Cepstral Coefficients (MFCC), Linear Predictive Codes (LPC), Perceptual Linear Prediction (PLP), or PLP-RASTA (PLP-Relative Spectra) \autocite{ittichaichareon2012:asr_using_mfcc} \autocite{dave2013:asr_feature_extraction} \autocite{dhingra2013:asr_using_mfcc}.

However, with recent advances in hardware and software performance, operating on raw input data has been shown to be extremely effective. Instead of feature engineering being done as a separate step before model training commences, models can instead be provided with raw input. This allows fitting models end-to-end, enabling them to essentially engineer their own features. Instead of each step of the pipeline having to be optimized separately, the whole system can be trained in unison. This often leads to superior performance, because effects of changes can be calculated across component boundaries. Additionally, mathematical optimization techniques can be applied to all modules, eliminating the need for manual intervention. \autocite{deng2012:asr:end_to_end_learning} \autocite{mnih2013:deep_reinforcement_learning:end_to_end} \autocite{glasmachers2017:end_to_end_learning} \autocite{tds2019:end_to_end_learning}

In this article, we address the lack of research concerning the utilization of autoencoders in ASR, by presenting a quantitative analysis of their performance when applied end-to-end to raw audio data. Section \ref{sec:methods} covers the architecture of the models we used, their training process, and the evaluation techniques which were employed. Obtained results are presented in section \ref{sec:results}, and finally discussed in section \ref{sec:discussion}.

\section{Methods}
\label{sec:methods}

Expounding all of the architectural and hyper-parameter choices is unfortunately outside of the scope of this article. Thus, this section is limited to cursorily covering and justifying the decisions which were made, without going into too much detail.

\subsection{Model Architecture}

All models were implemented, trained, and evaluated using TensorFlow \autocite{abadi2016:tensorflow}. Code to reproduce the results can be found in our GitLab repository \autocite{werner2021:code}.

\begin{figure}[!ht]
	\centering
	\begin{subfigure}{0.5\linewidth}
		\includegraphics[width=\linewidth]{plots/enc-architecture}
		\caption{Encoder Architecture}
		\label{fig:enc_architecture}
	\end{subfigure}%
	\begin{subfigure}{0.5\linewidth}
		\includegraphics[width=\linewidth]{plots/dec-architecture}
		\caption{Decoder Architecture}
		\label{fig:dec_architecture}
	\end{subfigure}
	\caption{Encoder and Decoder}
\end{figure}

The encoder and decoder architectures, depicted in figures \ref{fig:enc_architecture} and \ref{fig:dec_architecture} respectively, are fairly conventional. 8192-dimensional audio data entering the encoder is reshaped to match the input shapes expected by the convolutional layers, and then traverses three consecutive blocks consisting of a convolutional layer followed by average pooling. A final convolution layer merges the different channels before they are flattened to get the 1024-dimensional code. This structure is basically mirrored for the decoder, which produces a 8192-dimensional reconstruction from the 1024-dimensional code.

\begin{figure}[!ht]
	\centering
	\begin{subfigure}{0.5\linewidth}
		\includegraphics[width=\linewidth]{plots/ae-architecture}
		\caption{Autoencoder Architecture}
		\label{fig:ae_architecture}
	\end{subfigure}%
	\begin{subfigure}{0.5\linewidth}
		\includegraphics[width=\linewidth]{plots/cls-architecture}
		\caption{Classifier Architecture}
		\label{fig:cls_architecture}
	\end{subfigure}
	\caption{Autoencoder and Classifier}
\end{figure}

Figure \ref{fig:ae_architecture} shows that the autoencoder uses a standard architecture, like shown in the schema from figure \ref{fig:ae_schema}. It simply consists of an encoder followed by a decoder. The 8192-dimensional original audio sample is fed into the encoder, which produces a 1024-dimensional code that is used by the decoder to construct a 8192-dimensional reproduction.

Similarly, the classifier (figure \ref{fig:cls_architecture}) also follows a pretty standard layout. Instead of operating on the raw audio, it uses the encoder to turn the 8192-dimensional original audio into a 1024-dimensional code, which is then interpreted by a dense (fully-connected) layer before being turned into a one-hot probability distribution by the softmax-activated dense output layer.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\linewidth]{plots/activation_functions}
	\caption{Activation Functions}
	\label{fig:activation_functions}
\end{figure}

Several activation functions were tested, which are depicted in figure \ref{fig:activation_functions}. The identity activation function $\sigma_I$ was found to exhibit superior performance (see section \ref{sec:results:activation_functions}), and was consequently adopted as the activation function of the encoder and decoder.

\subsection{Training}

\subsubsection{Datasets}
\label{sec:methods:datasets}

Two datasets were used for training, one of them a synthetic sine wave dataset, and another comprising real audio recordings. All of the training data was in the range $[-1, 1]$, with 8192 samples, and a sampling rate of 8000 samples per second.

\begin{description}
	\item[Sine Wave Dataset] Joseph Fourier discovered that any periodic wave form can be approximated with arbitrary precision as a sum of sine waves \autocite{fourier1822:fourier_analysis}, so sine waves can be seen as the underlying waveform of all audio. Therefore, we decided to generate a synthetic sine wave dataset to train the autoencoder, in the hope that this generalizes such that a model which can reproduce individual sine waves is able to reproduce sums of those waves as well.

	Sine waves are described by the function $f(x) = A \sin (2 \pi f + \phi)$, where $A$ is the amplitude, $f$ the frequency, and $\phi$ the phase. $A$ determines how much the wave changes during a period, $f$ controls the length of periods, and $\phi$ shifts the starting point of the period.

	A total of 21654 synthetic sine waves were generated from $A \times f \times \phi$, where

	\[\begin{aligned}
		A &= \{\frac{5}{10}, \frac{6}{10}, \dots, \frac{10}{10}\}\\
		f &= \{0, 1, \dots, 400\}\\
		\phi &= \{\frac04 \pi, \frac14 \pi, \dots, \frac84 \pi\}\\
	\end{aligned}\]
	\item[Spoken Digit Dataset] To get representative real-world speech data, the Free Spoken Digit Dataset (FSDD) \autocite{jackson2016:spoken_digit_dataset} included with TensorFlow Datasets was used. It consists of audio recordings of spoken digits by 5 speakers with 50 examples per speaker per digit, totaling 2500 recordings. By now, the dataset has added a sixth speaker, who is not yet included in the TensorFlow Datasets version.

	Those audio recordings have variable lengths and amplitudes, and were therefore preprocessed. First, recordings longer than 8192 samples were filtered out, leaving us with 2496 recordings, which were then scaled to the range $[-1, 1]$. Finally, all recordings were zero padded to the right, to get a fixed number of samples.
\end{description}

\subsubsection{Hyperparameters}

Extensive Hyperparameter optimization might be an interesting topic for future research (see section \ref{sec:discussion}), but unfortunately fell outside of the scope of this article. Therefore, we mainly went with "obvious" popular choices for most of them, which are listed in table \ref{tab:hyperparameters}.

\begin{table}
	\centering
	\begin{tabular}{r||cc}
		\hline
		\multicolumn{1}{c||}{\textbf{Parameter}} & \multicolumn{2}{c}{\textbf{Model}}\\
		& \textbf{Autoencoder} & \textbf{Classifier}\\
		\hline
		\hline
		\textbf{Loss Function} & Mean Squared Error & Categorical Cross-Entropy\\
		\textbf{Optimizer} & Adam & Adam\\
		\textbf{\# of epochs} & 50 & 50\\
		\textbf{Learning Rate} & see figure \ref{fig:lr_schedule} & see figure \ref{fig:lr_schedule}\\
		\textbf{Batch Size} & 512 & 512\\
		\textbf{Dropout Rate} & - & $\frac14$\\
		\textbf{Activation Regularization} & - & -\\
		\textbf{Normalization} & - & -\\
		\textbf{Weight Decay} & - & -\\
		\textbf{Weight Regularization} & - & -\\
		\hline
	\end{tabular}
	\caption{Hyperparameter Overview}
	\label{tab:hyperparameters}
\end{table}

As you can see, there is probably still lots of rooms for improvement, as none of the choices apart from the learning rate have been systematically evaluated or optimized. There are even several techniques which have not been applied at all, like regularization, normalization, and weight decay.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\linewidth]{plots/lr_schedule}
	\caption{Learning Rate Schedule}
	\label{fig:lr_schedule}
\end{figure}

Figure \ref{fig:lr_schedule} shows the learning rate schedule which was used for all training runs. It has a horizon of 50 episodes, including warm-up and cool-down, with a maximum learning rate of $\eta = 0.001$. Addition of a warm-up phase stabilized results across multiple runs, such that the variance decreased, and adding a cool-down period sightly improved final model performance.

\subsection{Evaluation}

\subsubsection{Model Training}

Metrics were tracked during the training of the autoencoder and classifier. This enables us to plot them over time to see how the models learn, and to spot potential problems which might arise, like overfitting or forgetting. Diagrams of the training histories are included in figures \ref{fig:ae_training} and \ref{fig:cls_pretraining}.

Autoencoder training was split into two eras, to further check whether training on sine waves generalizes to complex waves and whether the model retains previously learned knowledge well. Both eras consist of 50 episodes each, following the learning rate schedule from figure \ref{fig:lr_schedule}. During the first era, the autoencoder is trained exclusively on sine waves, during the second era its trained solely on the spoken digit dataset. Meanwhile, the reproduction loss on both datasets was recorded across both eras.

Training the classifier twice, with and without pretraining the encoder, reveals whether useful features for classification are extracted by the autoencoder. This was done by saving the weights exclusive to the classifier, training it on the spoken digit dataset, then loading the pretrained encoder weights and restoring the saved classifier weights, and finally retraining the classifier on the spoken digit dataset. The results are pictured in figure \ref{fig:cls_pretraining}.

\subsubsection{Model Performance}
\label{sec:methods:evaluation:model_performance}

If the code representation of audios extracted by the encoder contains the essential features of those waves, a classifier trained on these codes should be able to predict attributes of the original audio with high accuracy. This was analyzed by fitting the classifier (see figure \ref{fig:cls_architecture}) to the spoken digit dataset, and plotting the confusion matrix from figure \ref{fig:cls-digits-confusion_matrix}.

We also wanted to measure the performance of the autoencoder when applied to the spoken digit dataset. This gives data for cross-referencing the results from the previous classifier evaluation, and is also generally interesting when it comes to generalizability of the model. To this end, we split the dataset into individual digits, and calculated the MSE for each group. The results of this were visualized in figure \ref{fig:ae-digits-mse_per_digit}.

In order to quantify the effect of different sine wave parameters on reproduction performance, another synthetic sine wave dataset was generated from $A \times f \times \phi$, where

\[\begin{aligned}
	A &= \{\frac{50}{100}, \frac{51}{100}, \dots, \frac{100}{100}\}\\
	f &= \{0, 1, \dots, 400\}\\
	\phi &= \{\frac04 \pi, \frac14 \pi, \dots, \frac84 \pi\}\\
\end{aligned}\]

Evaluating the Mean Squared Error for all sines results in a three-dimensional tensor of metric values. This allows us to plot the reproduction performance as a function of sine wave parameters. Figures \ref{fig:ae-sines-mse_amplitude}, \ref{fig:ae-sines-mse_frequency}, and \ref{fig:ae-sines-mse_phase} illustrate the influence of single parameters, and were generated by taking the mean of the metric tensor over the remaining two parameters. Averaging over only one of the axes produces a matrix showing how combinations of values for the other two parameters affect reproduction performance, as depicted in figures \ref{fig:ae-sines-mse_amplitude_frequency}, \ref{fig:ae-sines-mse_amplitude_phase}, and \ref{fig:ae-sines-mse_frequency_phase}.

As discussed earlier, simple sine waves are defined by only three properties, thus not a lot of information is needed to encode them perfectly. However, more complex waveforms, which can bee seen as sums of simple sine waves (see section \ref{sec:methods:datasets}), require more information. If the model managed to overfit on the sine wave dataset, by extracting those three defining properties, it should have trouble reproducing waves whose properties are dynamically changing. Moreover, we hoped that training on simple sine waves would generalize to complex waves.

To test both of these things, figure \ref{fig:ae-sines-dynamic_properties} was generated, by taking sine waves with dynamically changing properties, and overlaying the reproductions output by the autoencoder. First, amplitude $A$, frequency $f$, and phase $\phi$ were varied individually, while keeping the other two parameters fixed, then all of them were changed at the same time.

Complex waves being decomposable into sums of simpler waves gives rise to an interesting question: Are the codes extracted from audio by the encoder also composable? In other words: Is the autoencoder addition invariant? Mathematically expressed, given two waves $s_1, s_2$, encoder $E$, and decoder $D$, does $D(E(s_1) + E(s_2)) = D(E(s_1 + s_2)) \approx s_1 + s_2$ hold? This question was addressed empirically by taking pairs of simple sines, and testing whether this property holds, which is how figure \ref{fig:ae-addition_invariance} was created.

\section{Results}
\label{sec:results}

\subsection{Activation Functions}
\label{sec:results:activation_functions}

$\sigma_{elu}$, $\sigma_{gelu}$, and $\sigma_{relu}$ are popular activation functions which were tried for thoroughness, though they were not expected to perform well, due to the nature of the audio data. Indeed, their performance was on the order of three magnitudes worse compared to the identity activation function $\sigma_I$, predictably having particular trouble with troughs in the waveforms.

We considered $2 * \sigma_\sigma - 1$ and $\sigma_{\tanh}$ promising, because of their codomain matching the training datasets. This assumption proved to be correct insofar as they performed two orders of magnitude better than the previously mentioned functions. Nonetheless, they were still ten times worse than the identity function.

When we noticed that the autoencoder would not output values outside of the range $(-1, 1)$, an attempt was made to address this with the custom activation function $\sigma_{\log(x + 1)}$, which is defined as $\sigma_{\log(x + 1)}(x) = \log(|x| + 1) * \sign x$. To our dismay however, this diminished performance rather than increase it.

\subsection{Model Training}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\linewidth]{plots/ae-sines_digits-training_history}
	\caption{Autoencoder Training History}
	\label{fig:ae_training}
\end{figure}

Figure \ref{fig:ae_training} demonstrates that the autoencoder learns to extract generic features, which generalize to more complex wave forms; and that it manages to retain performance on one dataset, while being trained on another. Those results are exactly what we had aspired to.

During the first era, while training on the sine wave dataset, the autoencoder reduces its reproduction loss for both datasets. This confirms our hope that it should learn to reproduce more complex waveforms by training solely on sine waves. After some time, the training loss on the spoken digit dataset plateaus and even slightly increases, indicating that the autoencoder is overfitting on the sine wave dataset. This is rectified during the second era, throughout which the spoken digit dataset is used, improving the model's performance on it while retaining performance on the sine wave dataset.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\linewidth]{plots/cls-pretraining}
	\caption{Efficacy of Classifier Pretraining}
	\label{fig:cls_pretraining}
\end{figure}

Our expectations were not met when it comes to the efficacy of pretraining the classifier, which does not appear to benefit from the weights learned by the autoencoder, as illustrated by figure \ref{fig:cls_pretraining}. Instead of converging faster and/or reaching higher accuracy or lower loss, the classifier converges slower when starting from a pretrained encoder, as compared to starting from scratch. Eventually, the classifier reaches perfect accuracy anyway (see figure \ref{fig:cls-digits-confusion_matrix}), but that seems to happen in spite of the pretraining instead of benefiting from it.

\subsection{Model Performance}
\label{sec:results:model_performance}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.75\linewidth]{plots/cls-digits-confusion_matrix}
	\caption{Confusion Matrix - Spoken Digit Classification}
	\label{fig:cls-digits-confusion_matrix}
\end{figure}

Operating on top of the encoder, the classifier was able to perfectly predict all digits in the spoken digit dataset (see figure \ref{fig:cls-digits-confusion_matrix}), suggesting that the code extracted by the encoder is information dense, capturing the important essential characteristics of the input waves. Note that this result is not indicative of the classifier's performance in general, as the confusion matrix was generated for the test set, and as a result probably merely measures overfitting. However, this does not matter, as the goal of this assessment was to demonstrate that the code representation is able to capture sufficient information, which it does.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\linewidth]{plots/ae-digits-mse_per_digit}
	\caption{Autoencoder Reproduction Loss per Spoken Digit}
	\label{fig:ae-digits-mse_per_digit}
\end{figure}

Spoken digits -- as a stand-in for complex, real-world audio data -- can be reproduced by the autoencoder with high accuracy, as evidenced by figure \ref{fig:ae-digits-mse_per_digit}. Qualitative analysis confirms these results. Listening to the reproductions, it is harder to make out numbers with higher reproduction loss, especially digits 4 and 5. Still, a majority can be easily understood, even after reproduction.

\begin{figure}[!ht]
	\begin{subfigure}{\linewidth}
		\centering
		\includegraphics[width=0.8\linewidth]{plots/ae-sines-mse_amplitude}
		\caption{Function of Amplitude $A$}
		\label{fig:ae-sines-mse_amplitude}
	\end{subfigure}
	\begin{subfigure}{\linewidth}
		\centering
		\includegraphics[width=0.8\linewidth]{plots/ae-sines-mse_frequency}
		\caption{Function of Frequency $f$}
		\label{fig:ae-sines-mse_frequency}
	\end{subfigure}
	\begin{subfigure}{\linewidth}
		\centering
		\includegraphics[width=0.8\linewidth]{plots/ae-sines-mse_phase}
		\caption{Function of Phase $\phi$}
		\label{fig:ae-sines-mse_phase}
	\end{subfigure}
	\caption{Autoencoder Reproduction Loss as a Function of Sine Wave Parameters}
\end{figure}

Inherently, sines with increased amplitudes result in larger MSE scores due to higher magnitude values, which contribute more towards total MSE. Because values are squared, the performance decrease is superlinear, as shown in figure \ref{fig:ae-sines-mse_amplitude}. Apart from this effect, which was expected, the amplitude does not appear to have an appreciable effect on the reproduction performance, at least in the range which was evaluated.

Judging from figure \ref{fig:ae-sines-mse_frequency}, frequency has the highest influence on autoencoder reproduction performance. This makes sense, considering that high frequency data carries more information per time span. Below a threshold of about $f = 360$, waves are reproduced with low error, but after that cut-off point the MSE explodes. Ostensibly, with the feature extraction techniques learned by the model, it is not able to capture such high frequency information in its code representation, leading to bad reproduction results.

Figure \ref{fig:ae-sines-mse_phase} nicely demonstrates a shortcoming of the autoencoder architecture. MSE is lowest at $\pi | \phi$ ($\phi$ is divisible by $\pi$), because then the starting point of the sine waves is 0, matching the zero padding being applied by the convolutional layers. It is highest right in between those points (e.g. at $\phi = \frac\pi2$), due to having the highest magnitude starting and end points, resulting in large errors at both ends of the audio snippets. This effect can be observed in figure \ref{fig:ae-addition_invariance}, where reproductions always sharply drop towards 0 at both edges.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\linewidth]{plots/ae-sines-mse_amplitude_frequency}
	\caption{Autoencoder Reproduction Loss as a Function of Amplitude $A$ and Frequency $f$}
	\label{fig:ae-sines-mse_amplitude_frequency}
\end{figure}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\linewidth]{plots/ae-sines-mse_amplitude_phase}
	\caption{Autoencoder Reproduction Loss as a Function of Amplitude $A$ and Phase $\phi$}
	\label{fig:ae-sines-mse_amplitude_phase}
\end{figure}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\linewidth]{plots/ae-sines-mse_frequency_phase}
	\caption{Autoencoder Reproduction Loss as a Function of Frequency $f$ and Phase $\phi$}
	\label{fig:ae-sines-mse_frequency_phase}
\end{figure}

Looking at figures \ref{fig:ae-sines-mse_amplitude_phase} and \ref{fig:ae-sines-mse_frequency_phase}, phase $\phi$ is apparently largely independent of the other two properties. Fixing a value for $A$ or $f$ and varying $\phi$ does not have an influence above what you would expect for any other value of $A$ or $f$.

Only amplitude and frequency are strongly correlated, as illustrated by figure \ref{fig:ae-sines-mse_amplitude_frequency}. Fixing $A = \frac23$, for example, the influence of varying $f$ is much lower than for $A = 1$.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\linewidth]{plots/ae-sines-dynamic_properties_overlay}
	\caption{Autoencoder Reproduction Loss on Sines with Dynamic Properties}
	\label{fig:ae-sines-dynamic_properties}
\end{figure}

Dynamically changing properties are handled gracefully by the autoencoder, even though it has only been trained on simple sine waves with fixed attributes. Overlaying originals and reproductions as in figure \ref{fig:ae-sines-dynamic_properties} reveals that the autoencoder can handle both single and multiple dynamically changing properties at one, generating faithful reproductions in all cases.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\linewidth]{plots/ae-addition_invariance}
	\caption{Addition Invariance of Code Representations}
	\label{fig:ae-addition_invariance}
\end{figure}

Figure \ref{fig:ae-addition_invariance} shows that the autoencoder is indeed addition invariant, which is a nice property to have. In fact, $E(s_1 + s_2)$ and $E(s_1) + E(s_2)$ are so close that they are somewhat hard to distinguish without zooming in.

\[E(s_1 + s_2) \approx E(s_1) + E(s_2)\]

such that

\[D(E(s_1 + s_2)) \approx D(E(s_1) + E(s_2)) \approx s_1 + s_2\]

which is what we wanted to demonstrate.

\section{Discussion}
\label{sec:discussion}

\subsection{Conclusions}

We have shown autoencoders to be extremely versatile and easy to use. Due to their genericity, they can be applied in almost any domain. Training can be done quickly and cheaply in an unsupervised manner, making them a tool of choice. Due to their architecture, they autonomously learn to extract essential features of the input data, providing a good description of it, which can also be useful for other tasks.

Furthermore, we have shown that autoencoders trained on simple sine waves generalize well to dynamic sine waves whose attributes change over time, and even to complex real world audio recording of speech. They also manage to retain previously learned information tremendously well while being trained on another dataset. Lastly, they have the useful property of being addition invariant, such that the code of sums of waves is (almost) equal to the sum of codes of waves.

These conclusions were drawn based on extensive quantitative analysis of the training and inference performance of autoencoders and auxiliary models making use of their components (e.g. encoder-based classifier), taking the effects of model and data parameters into account.

\subsection{Problems}

$\sigma_I$ outperformed all other activations functions in our tests, as documented in section \ref{sec:results:activation_functions}. This seems strange to us, as activation functions are normally used to enable neural networks to learn complex nonlinear functions \autocite{tds2019:activation_functions}. We suspect that our model architecture is far from optimal, and that an autoencoder with larger capacity (i.e. more parameters) would be able to benefit from nonlinear activation functions like $\sigma_{\tanh}$ or $\sigma_{\log(x + 1)}$.

A somewhat related problem is that we still think that $\sigma_{\log(x + 1)}$ should outperform $\sigma_{\tanh}$ and $2 * \sigma_\sigma + 1$, despite empirical evidence suggesting otherwise. Both $\sigma_{\tanh}$ and $2 * \sigma_\sigma + 1$ exclusively output values in the range $(-1, 1)$, which severely limits the waveforms models based on them can reproduce.

$\sigma_{\log(x + 1)}$ is similar to those functions around $-1 < x < 1$, but does not have an upper or lower limit on the values it can output. Therefore, we expected it to outperform them. This not being the case may be caused by a problem with differentiability, as $\sigma_{\log(x + 1)}$ uses both $abs$ and $sign$ functions. If that is correct, it should be fixable with a custom gradient.

Another problem we encountered was technical in nature, in that TensorFlow cannot compute the cube root of negative numbers, and outputs NaN instead. Therefore, we could not try the activation function $\sigma_{x^\frac13}(x) = x^\frac13$.

\subsection{Future Research}

We consider Itakura–Saito distance a promising candidate for use as a loss function and/or metric, as it was specifically designed to measure the quality of spectral approximations, fitting the audio use case very well.

Due to the zero padding required to make the tensor shapes work out, artifacts are introduced at the beginning and end of audio data put through the autoencoder. This could potentially be resolved by either padding with the left- and rightmost value respectively instead of zero, or alternatively (maybe preferably) by eliminating the need for padding altogether, through the use of a different model architecture.

Visualizing the convolution kernels and layer activations might be interesting as well, in order to gain an intuition about what the autoencoder is doing. This may reveal possibilities for improvement, and could make it possible to extract insights the model might have gained.

A lot of extensions to simple autoencoders have been explored in the literature, for example Variational AutoEncoders (VAEs) or Relational Autoencoders \autocite{meng2017:autoencoder:feature_extraction}. We have limited ourselves to simple standard autoencoders for simplicity, but considering and comparing alternatives would probably make for an interesting follow-up.

As stated before, the autoencoder architecture and hyperparameters used in this article are largely unoptimized. Measuring and quantifying their influence could lead to big increases in performance, potentially even to a positive feedback loop between optimizing parameters and hyperparameters.

In general, a lot more things could be measured and quantified. For example, in section \ref{sec:results:model_performance} it was mentioned that phase $\phi$ seems independent of amplitude $A$ and frequency $f$, while $A$ and $f$ are dependent. One could compute the covariance matrix between amplitude, frequency, and phase to gage the strength of this correlation.

\clearpage
\printbibliography

\end{document}
