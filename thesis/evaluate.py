import sys
from functools import partial
from itertools import combinations

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow import keras

# make kwgen importable
sys.path.append('.')

from kwgen import config  # noqa: E402
from kwgen.data import (  # noqa: E402
    sine,
    sine_dataset_factory,
    sine_generator,
    spoken_digit_dataset_factory,
    π,
)
from kwgen.evaluation import (  # noqa: E402
    draw_confusion_matrix,
    draw_metric_plot,
    evaluate,
)
from kwgen.training import (  # noqa: E402
    autoencoder,
    classifier,
    decoder,
    encoder,
    sine_dataset as sine_dataset_train,
    spoken_digit_dataset as spoken_digit_dataset_train,
    train,
)
from kwgen.util import (  # noqa: E402
    moving_average,
    scale,
)


def evaluate_autoencoder_on_sine_dataset(model):
    mean_squared_error = (
        evaluate(model, sine_dataset, keras.metrics.MSE)
        .reshape((n_amplitudes, n_frequencies, n_phases))
    )

    draw_metric_plot(
        mean_squared_error.mean(axis=2),
        title='MSE: Amplitude and Frequency',
        x_label='frequency',
        x_values=frequencies,
        y_label='amplitude',
        y_values=amplitudes,
        save_path='thesis/plots/ae-sines-mse_amplitude_frequency.pdf',
    )

    draw_metric_plot(
        mean_squared_error.mean(axis=0),
        title='MSE: Frequency and Phase',
        x_label='phase',
        x_values=phases,
        y_label='frequency',
        y_values=frequencies,
        save_path='thesis/plots/ae-sines-mse_frequency_phase.pdf',
    )

    draw_metric_plot(
        mean_squared_error.mean(axis=1),
        title='MSE: Amplitude and Phase',
        x_label='phase',
        x_values=phases,
        y_label='amplitude',
        y_values=amplitudes,
        save_path='thesis/plots/ae-sines-mse_amplitude_phase.pdf',
    )

    plt.figure(figsize=(8, 4))
    plt.title('MSE: Amplitude')
    plt.xlabel('amplitude')
    plt.ylabel('mean squared error')
    plt.plot(amplitudes, mean_squared_error.mean(axis=(1, 2)))
    plt.tight_layout()
    plt.savefig('thesis/plots/ae-sines-mse_amplitude.pdf')
    plt.show()

    plt.figure(figsize=(8, 4))
    plt.title('MSE: Frequency')
    plt.xlabel('frequency')
    plt.ylabel('mean squared error')
    plt.plot(frequencies, mean_squared_error.mean(axis=(0, 2)))
    plt.tight_layout()
    plt.savefig('thesis/plots/ae-sines-mse_frequency.pdf')
    plt.show()

    plt.figure(figsize=(8, 4))
    plt.title('MSE: Phase')
    plt.xlabel('phase')
    plt.ylabel('mean squared error')
    plt.plot(phases, mean_squared_error.mean(axis=(0, 1)))
    plt.tight_layout()
    plt.savefig('thesis/plots/ae-sines-mse_phase.pdf')
    plt.show()

    print('mean MSE on sine wave dataset:', mean_squared_error.mean())

    return mean_squared_error


def evaluate_autoencoder_on_spoken_digit_dataset(model):
    metric_values, scale_exponent = scale(np.array([
        model.evaluate(dataset)
        for dataset in spoken_digit_datasets__reproduction
    ]))

    plt.figure(figsize=(8, 4))
    plt.title('MSE per spoken digit')
    plt.xlabel('digit')
    plt.ylabel(f'mean squared error * $10^{{{scale_exponent}}}$')
    plt.xticks(range(10))
    plt.bar(range(10), metric_values)
    for i, metric_value in enumerate(metric_values):
        plt.annotate(
            f'{metric_value:.3f}',
            (i, metric_value),
            color='white',
            horizontalalignment='center',
            textcoords='offset points',
            verticalalignment='top',
            xytext=(0, -8),
        )
    plt.tight_layout()
    plt.savefig('thesis/plots/ae-digits-mse_per_digit.pdf')
    plt.show()

    print('mean MSE on spoken digit dataset:', metric_values.mean())

    return metric_values


def evaluate_autoencoder_on_dynamic_sines(
    model,
    metric=keras.metrics.MSE,
    metric_name='MSE',
):
    def interval(values):
        return r'[{}, {}]'.format(values[0], values[-1])

    def parameters(
        amplitude='A = 1',
        frequency='f = 1',
        phase=r'\phi = 0',
    ):
        return '$' + ', '.join((amplitude, frequency, phase)) + '$'

    dynamic_amplitude = np.linspace(-1, 1, num=config.AUDIO_LENGTH)
    dynamic_frequency = np.linspace(0, 50, num=config.AUDIO_LENGTH)
    dynamic_phase = np.linspace(0, 2 * π, num=config.AUDIO_LENGTH)

    originals = np.array([
        sine(amplitude=dynamic_amplitude, frequency=50),
        sine(frequency=dynamic_frequency),
        sine(phase=dynamic_phase),
        sine(amplitude=dynamic_amplitude, frequency=dynamic_frequency, phase=dynamic_phase),
    ])
    n_sines = len(originals)

    reproductions = model.predict(originals)
    metric_values, scale_exponent = scale(metric(originals, reproductions))

    interval_amplitude = fr'A \in {interval(dynamic_amplitude)}'
    interval_frequency = fr'f \in {interval(dynamic_frequency)}'
    interval_phase = r'\phi \in [0, 2\pi]'

    titles = (
        parameters(amplitude=interval_amplitude, frequency='f = 50'),
        parameters(frequency=interval_frequency),
        parameters(phase=interval_phase),
        parameters(
            amplitude=interval_amplitude,
            frequency=interval_frequency,
            phase=interval_phase,
        ),
    )

    figure = plt.figure(figsize=(10, 2 * n_sines + 0.5))
    figure.suptitle('Sines with Dynamic Properties')
    grid_spec = figure.add_gridspec(n_sines, 5)

    # overlay of originals and reproductions
    for i, (original, reproduction, title) in enumerate(zip(
        originals,
        reproductions,
        titles,
    )):
        ax = figure.add_subplot(grid_spec[i, :-1])
        ax.set_title(title)
        ax.plot(original, label='original')
        ax.plot(reproduction, label='reproduction')
    ax.legend()

    # bar plot with metric values
    ax = figure.add_subplot(grid_spec[:, -1])
    ax.set_title(f'{metric_name} * $10^{{{scale_exponent}}}$')
    ax.set_yticks(())
    ax.set_ylim(-0.5, n_sines - 0.5)
    ax.barh(range(n_sines), metric_values, height=0.5)
    for i, metric_value in enumerate(metric_values):
        ax.annotate(
            f'{metric_value:.3f}',
            (metric_value, i),
            color='white',
            horizontalalignment='right',
            textcoords='offset points',
            verticalalignment='center',
            xytext=(-8, 0),
        )

    figure.tight_layout()
    figure.savefig('thesis/plots/ae-sines-dynamic_properties_overlay.pdf')
    plt.show()

    return originals, reproductions, metric_values


def evaluate_autoencoder_addition_invariance(
    encoder,
    decoder,
    metric=keras.metrics.MSE,
    metric_name='MSE',
):
    def annotate_bar(x, y):
        plt.annotate(
            f'{x:.3f}',
            (x, y),
            color='white',
            horizontalalignment='right',
            rotation=-90,
            textcoords='offset points',
            verticalalignment='center',
            xytext=(-8, 0),
        )

    pairs = partial(combinations, r=2)

    sines = np.array(list(sine_generator(
        amplitudes=(0.7,),
        frequencies=(1, 3),
        phases=(0, 0.5 * π),
    )))

    sine_sums = np.array([sine1 + sine2 for sine1, sine2 in pairs(sines)])

    sine_codes = encoder.predict(sines)
    sine_sum_codes = encoder.predict(sine_sums)

    sine_code_sums = np.array([code1 + code2 for code1, code2 in pairs(sine_codes)])

    sine_code_sum_reproductions = decoder.predict(sine_code_sums)
    sine_sum_code_reproductions = decoder.predict(sine_sum_codes)

    (metric_code_sum, metric_sum_code), scale_exponent = scale((
        metric(sine_sums, sine_code_sum_reproductions),
        metric(sine_sums, sine_sum_code_reproductions),
    ))

    n_pairs = len(sine_sums)

    def legend_and_ticks(i, handles=(), labels=()):
        if i == 0 and handles and labels:
            plt.legend(handles, labels)

        if 0 < i < (n_pairs - 1):
            plt.xticks(())

    figure = plt.figure(figsize=(15, 2 * n_pairs))
    grid_spec = figure.add_gridspec(n_pairs, 5)
    figure.suptitle('Addition Invariance')
    for i, (
        sine_code_sum,
        sine_code_sum_repr,
        sine_sum,
        sine_sum_code,
        sine_sum_code_repr,
        (idx1, idx2),
    ) in enumerate(zip(
        sine_code_sums,
        sine_code_sum_reproductions,
        sine_sums,
        sine_sum_codes,
        sine_sum_code_reproductions,
        pairs(range(len(sine_codes))),
    )):
        ax = figure.add_subplot(grid_spec[i, :2])
        ax.plot(sine_codes[idx1], color='r', label='$E(s_1)$', linestyle=':')
        ax.plot(sine_codes[idx2], color='g', label='$E(s_2)$', linestyle=':')
        handles1, labels1 = ax.get_legend_handles_labels()
        legend_and_ticks(i)
        ax = ax.twiny()
        ax.plot(sines[idx1], color='r', label='$s_1$')
        ax.plot(sines[idx2], color='g', label='$s_2$')
        handles2, labels2 = ax.get_legend_handles_labels()
        legend_and_ticks(i, handles1 + handles2, labels1 + labels2)

        ax = figure.add_subplot(grid_spec[i, 2:4])
        ax.plot(sine_code_sum, color='y', label='$E(s_1) + E(s_2)$', linestyle=':')
        ax.plot(sine_sum_code, color='b', label='$E(s_1 + s_2)$', linestyle=':')
        handles1, labels1 = ax.get_legend_handles_labels()
        legend_and_ticks(i)
        ax = ax.twiny()
        ax.plot(sine_code_sum_repr, color='y', label='$D(E(s_1) + E(s_2))$')
        ax.plot(sine_sum_code_repr, color='b', label='$D(E(s_1 + s_2))$')
        ax.plot(sine_sum, color='black', label='$s_1 + s_2$', linestyle='--')
        handles2, labels2 = ax.get_legend_handles_labels()
        legend_and_ticks(i, handles1 + handles2, labels1 + labels2)
    bar_height = 0.4
    y_code_sum = np.arange(n_pairs) - bar_height / 2
    y_sum_code = np.arange(n_pairs) + bar_height / 2
    ax = figure.add_subplot(grid_spec[:, -1])
    ax.set_title(f'{metric_name} * $10^{{{scale_exponent}}}$')
    ax.set_ylim(-bar_height, n_pairs - 1 + bar_height)
    ax.set_yticks(())
    ax.barh(y_code_sum, metric_code_sum, color='y', height=bar_height)
    ax.barh(y_sum_code, metric_sum_code, color='b', height=bar_height)
    for (
        y_code_sum,
        metric_value_code_sum,
        y_sum_code,
        metric_value_sum_code,
    ) in zip(
        y_code_sum,
        metric_code_sum,
        y_sum_code,
        metric_sum_code,
    ):
        annotate_bar(metric_value_code_sum, y_code_sum)
        annotate_bar(metric_value_sum_code, y_sum_code)
    figure.tight_layout()
    figure.savefig('thesis/plots/ae-addition_invariance.pdf')
    plt.show()

    return sines, sine_code_sum_reproductions, sine_sum_code_reproductions


def evaluate_autoencoder_training(model):
    class EvalHistory(keras.callbacks.History):
        """History callback for evaluation."""
        def on_epoch_end(self, epoch, logs=None):
            logs = {} if logs is None else logs

            logs['mse_digits'] = self.model.evaluate(spoken_digit_dataset_train__reproduction)
            logs['mse_sines'] = self.model.evaluate(sine_dataset_train)

            super().on_epoch_end(epoch, logs)

    era_label = partial(
        plt.annotate,
        horizontalalignment='center',
        textcoords='offset points',
        verticalalignment='top',
        xytext=(0, -8),
    )
    eval_train = partial(train, callbacks=(EvalHistory(),))

    eval_train(model, sine_dataset_train)
    history = eval_train(model, spoken_digit_dataset_train__reproduction)

    mse = model.evaluate(sine_dataset_train)
    model.save_weights(f'data/weights/ae-sines_digits-mse{int(mse * 1e6):07d}.h5')

    era_length = history.params['epochs']

    plt.figure(figsize=(8, 4))
    plt.title('Training History')
    plt.xlabel('epoch')
    plt.ylabel('mean squared error')
    plt.plot(history.history['mse_digits'], color='r', label='spoken digits')
    plt.tick_params('y', labelcolor='r')
    handles1, labels1 = plt.gca().get_legend_handles_labels()
    plt.twinx()
    plt.plot(history.history['mse_sines'], color='b', label='sine waves')
    ylim = plt.ylim()
    plt.plot([era_length] * 2, ylim, color='black', linestyle='--')
    era_label('era 1: sine waves', (era_length * (1/2), ylim[1]))
    era_label('era 2: spoken digits', (era_length * (3/2), ylim[1]))
    plt.ylim(ylim)
    plt.tick_params('y', labelcolor='b')
    handles2, labels2 = plt.gca().get_legend_handles_labels()
    plt.legend(handles1 + handles2, labels1 + labels2, loc='center right')
    plt.tight_layout()
    plt.savefig('thesis/plots/ae-sines_digits-training_history.pdf')
    plt.show()

    return history


def evaluate_classifier_on_spoken_digit_dataset(model):
    metrics = model.evaluate(spoken_digit_dataset__classification)

    print('classifier metrics on spoken digit dataset:')
    for metric_name, metric in zip(model.metrics_names, metrics):
        print(f'\t{metric_name}: {metric}')

    labels = np.hstack([batch[1] for batch in spoken_digit_dataset__classification])
    predictions = model.predict(spoken_digit_dataset__classification).argmax(axis=-1)

    confusion_matrix = tf.math.confusion_matrix(labels, predictions)
    draw_confusion_matrix(
        confusion_matrix,
        save_path='thesis/plots/cls-digits-confusion_matrix.pdf',
        title='Confusion Matrix: Spoken Digit Classification',
    )

    return metrics, confusion_matrix


def evaluate_classifier_pretraining(model, layers=(-3, -1), colors='rbg'):
    def line(*args, **kwargs):
        return mpl.lines.Line2D((0,), (0,), *args, **kwargs)

    # save initial classifier weights
    weights = tuple(model.layers[layer].get_weights() for layer in layers)

    histories = [train(model, spoken_digit_dataset_train)]

    # load trained encoder weights
    autoencoder.load_weights('data/weights/autoencoder-trained')
    # restore initial classifier weights
    for layer, weight in zip(layers, weights):
        classifier.layers[layer].set_weights(weight)

    histories.append(train(model, spoken_digit_dataset_train))

    plt.figure(figsize=(8, 4))
    plt.title('Classifier Pretraining')
    plt.xlabel('epoch')
    plt.ylabel('training loss')
    for history, color in zip(histories, colors):
        loss = history.history['loss']
        plt.plot(loss, color=color, linestyle=':')
        plt.plot(moving_average(loss, 5), color=color)
    plt.twinx()
    plt.ylabel('accuracy')
    for history, color in zip(histories, colors):
        accuracy = history.history['accuracy']
        plt.plot(accuracy, color=color, linestyle=':')
        plt.plot(moving_average(accuracy, 5), color=color)
    plt.legend((
        line(color='black', linestyle=':'),
        line(color='black'),
        line(color=colors[0]),
        line(color=colors[1]),
    ), (
        'loss/accuracy',
        'loss/accuracy SMA(5)',
        'without pretraing',
        'with pretraing',
    ), loc='center right')
    plt.tight_layout()
    plt.savefig('thesis/plots/cls-pretraining.pdf')
    plt.show()

    return histories


n_amplitudes = 10
n_frequencies = 10
n_phases = 5

amplitudes = np.linspace(0.5, 1, num=n_amplitudes)
frequencies = np.linspace(0, 400, num=n_frequencies)
phases = np.linspace(0, 2 * π, num=n_phases)

sine_dataset = (
    sine_dataset_factory(
        amplitudes=amplitudes,
        frequencies=frequencies,
        phases=phases,
    )
    .batch(config.BATCH_SIZE)
    .prefetch(config.PREFETCH_BATCHES)
)

spoken_digit_dataset = spoken_digit_dataset_factory()
# inividual digits for reproduction
spoken_digit_datasets__reproduction = [
    spoken_digit_dataset
    .filter(lambda audio, label: label == digit)
    .padded_batch(config.BATCH_SIZE, ((config.AUDIO_LENGTH,), ()))
    .map(lambda audio, label: (audio, audio))
    .prefetch(config.PREFETCH_BATCHES)
    for digit in range(10)
]
# all digits for classification
spoken_digit_dataset__classification = (
    spoken_digit_dataset
    .padded_batch(config.BATCH_SIZE, ((config.AUDIO_LENGTH,), ()))
    .prefetch(config.PREFETCH_BATCHES)
)

spoken_digit_dataset_train__reproduction = spoken_digit_dataset_train.map(lambda a, l: (a, a))


if __name__ == '__main__':
    evaluate_autoencoder_training(autoencoder)

    autoencoder.load_weights('data/weights/autoencoder-trained')

    evaluate_autoencoder_on_sine_dataset(autoencoder)
    evaluate_autoencoder_on_spoken_digit_dataset(autoencoder)
    evaluate_autoencoder_on_dynamic_sines(autoencoder)

    evaluate_autoencoder_addition_invariance(encoder, decoder)

    evaluate_classifier_pretraining(classifier)

    classifier.load_weights('data/weights/classifier-trained')

    evaluate_classifier_on_spoken_digit_dataset(classifier)
