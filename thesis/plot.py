import sys

import matplotlib.pyplot as plt
import numpy as np
from tensorflow import keras

# make kwgen importable
sys.path.append('.')

from kwgen.training import (  # noqa: E402
    autoencoder,
    classifier,
    decoder,
    encoder,
    lr_schedule_factory,
)
from kwgen.util import log1p_activation  # noqa: E402


def plot_function(
    function,
    x=None,
    *,
    figsize=(8, 4),
    save_path=None,
    title=None,
    xlabel='$x$',
    ylabel='$f(x)$',
):
    x = np.linspace(-10, 10, num=10_000) if x is None else x
    y = function(x)

    plt.figure(figsize=figsize)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.plot(x, y)
    plt.tight_layout()
    if save_path is not None:
        plt.savefig(save_path)
    plt.show()


def plot_activation_functions():
    plt.figure(figsize=(10, 4))
    plt.suptitle('Activation Functions')
    plt.subplot(1, 2, 1)
    plt.xlabel('$x$')
    plt.ylabel(r'$\sigma(x)$')
    x = np.linspace(-10, 10, num=10_000)
    plt.plot(x, 2 * keras.activations.sigmoid(x) - 1, label=r'$2 * \sigma_\sigma - 1$')
    plt.plot(x, keras.activations.tanh(x), label=r'$\sigma_\tanh$')
    plt.plot(x, log1p_activation(x), label=r'$\sigma_{\log(x + 1)}$')
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.xlabel('$x$')
    plt.ylabel(r'$\sigma(x)$')
    x = np.linspace(-3, 3, num=10_000)
    plt.plot(x, x, label=r'$\sigma_I$')
    plt.plot(x, keras.activations.elu(x), label=r'$\sigma_{elu}$')
    plt.plot(x, keras.activations.gelu(x), label=r'$\sigma_{gelu}$')
    plt.plot(x, keras.activations.relu(x), label=r'$\sigma_{relu}$')
    plt.legend()
    plt.tight_layout()
    plt.savefig('thesis/plots/activation_functions.pdf')
    plt.show()


def plot_lr_schedule(epochs, *args, **kwargs):
    lr_schedule = lr_schedule_factory(*args, **kwargs)

    plot_function(
        lr_schedule,
        np.arange(epochs),
        title='Learning Rate Schedule',
        xlabel='epoch',
        ylabel='learning rate',
        figsize=(8, 2),
        save_path='thesis/plots/lr_schedule.pdf',
    )


def plot_model_architectures():
    for model, model_abbreviation, show_layer_names in (
        (autoencoder, 'ae', True),
        (classifier, 'cls', True),
        (decoder, 'dec', False),
        (encoder, 'enc', False),
    ):
        keras.utils.plot_model(
            model,
            dpi=300,
            show_layer_names=show_layer_names,
            show_shapes=True,
            to_file=f'thesis/plots/{model_abbreviation}-architecture.png',
        )


if __name__ == '__main__':
    plot_activation_functions()
    plot_lr_schedule(50, α=-2, β=1, γ=12, δ=3)
    plot_model_architectures()
