python_src = \
	kwgen\
	thesis/{evaluate,plot}.py\

.PHONY: lint
lint: lint_python

.PHONY: lint_python
lint_python:
	# Linting Python code…
	@pipenv run flake8 $(python_src)
	# Checking Python import order…
	@pipenv run isort --check $(python_src)
